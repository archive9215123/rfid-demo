URL: ec2-3-76-160-37.eu-central-1.compute.amazonaws.com
Protocol: mqtt://
Port: 1883
Topic: Area42PP/<seriale_macchina>/<id_dispositivo>
(seriale in /proc/cpuinfo, riga con stringa serial, split su :, ultimo valore trimmando whitespace)
Format: (Json)
[
    {
        "timestamp": int64, (timestamp in microsecondi -dall'inizio della connessione?-)
        "value": interface{}, (any type)
        "tag": String, (nome con cui viene segnato il value nel db)
        "type": String (tipo con cui interpretare il value, nel caso della temperatura float)
    }
]
