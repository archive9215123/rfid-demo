# rfid-demo
Codice prodotto per il progetto "Retrofitting di macchina industriale con sensori RFID" durante lo stage presso Santer Reply SPA

# Build
Il progetto si compone di due file jar, generati come artifact di IntelliJ, per buildarli: ```Build > Build Project``` o ```Build > Build Artifacts```.
Tutte le dipendenze sono indicate nel file pom.xml

# Utilizzo
rfid-daemon é pensato per essere utilizzato come daemon systemd, setuppando un servizio

```java -jar rfid-daemon.jar -s <serial-reader> -sk <socket-reader> [-v] -b <broker> -p <read-frequency>```

rfid-matchwrite trova un tag rfid con la stringa in input nei primi 8 byte della user memory e ne scrive un'altra

```java -jar rfid-matchwrite.jar -s <serial-reader> -sk <socket-reader> [-v] -m <string-match> -w <string-write>```