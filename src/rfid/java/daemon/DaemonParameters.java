package daemon;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

public class DaemonParameters {
    @Option(name = "-s", aliases = {"--serial"}, usage = "Define a serial reader to try to use")
    private String serialDevice;

    @Option(name = "-sk", aliases = {"--socket"}, usage = "Define an ip:port to connect to as reader")
    private String socket;

    @Option(name = "-v", aliases = {"--verbose"}, usage = "Print detailed log")
    private boolean verbose;

    @Option(name = "-b", aliases = {"--broker"}, usage = "MQTT broker address:port to send data to", required = true)
    private String broker;

    @Option(name = "-p", aliases = {"--period"}, usage = "Period to repeat read in milliseconds", required = true)
    private long period;

    private boolean correct = false;

    public void parse(String[] args) {
        CmdLineParser p = new CmdLineParser(this);
        try {
            p.parseArgument(args);
            correct = true;
        } catch (CmdLineException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public String getSerialDevice() {
        return serialDevice;
    }

    public String getSocket() {
        return socket;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public String getBroker() {
        return broker;
    }

    public long getPeriod() {
        return period;
    }

    public boolean areCorrect() {
        return correct;
    }
}
