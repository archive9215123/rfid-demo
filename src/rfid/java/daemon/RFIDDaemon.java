package daemon;

import com.nordicid.nurapi.*;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.json.JSONArray;
import util.APIBuilder;
import util.JSONMessageBuilder;
import util.MagnusS;
import util.MqttMessageSender;

import java.io.*;
import java.math.BigInteger;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.Timer;

// Main executable, perpetual read every 10 seconds

public class RFIDDaemon {

    public static void main(String[] args) {
        // Parse command line arguments, abort if errors
        DaemonParameters daemonParameters = new DaemonParameters();
        daemonParameters.parse(args);
        if (!daemonParameters.areCorrect()) {
            System.out.println("Error in parameters, aborting execution");
            return;
        }
        if (daemonParameters.getSerialDevice() == null && daemonParameters.getSocket() == null) {
            System.out.println("Error: no reader provided, use --socket or --serial");
            return;
        }
        // Get serial number, if not found abort
        String serial = getSerial();
        if (serial == null) {
            System.out.println("No serial found, aborting execution");
            return;
        }

        // Mqtt connection setup
        MqttMessageSender sender = null;
        try {
            sender = new MqttMessageSender("Area42PP/" + serial + "/", 2, daemonParameters.getBroker(), "raspberryA42", false);
        } catch (MqttException e) {
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("rfid.service " + ZonedDateTime.now(ZoneId.of("Europe/Rome")) + " Starting up, serial: " + serial);
        // util.MqttMessageSender spawns a thread, from now on if errors occur it should be killed with terminate()

        // API Connection and basic settings
        NurApi api = null; // Shouldn't be necessary but the compiler complains if it's not initialized because of the try/catch
        try {
            if (daemonParameters.getSerialDevice() != null) {
                api = APIBuilder.getAPISerial(daemonParameters.getSerialDevice(), daemonParameters.isVerbose());
            } else {
                api = APIBuilder.getAPISocket(daemonParameters.getSocket(), daemonParameters.isVerbose());
            }
        } catch (Exception e) {
            sender.terminate();
            e.printStackTrace();
            System.exit(1);
        }
        // API connection uses a thread, if errors happen should be killed with api.dispose()

        // Try scheduling timer to run read every minute
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new ScheduledTemperatureReadTask(api, sender, timer, daemonParameters.isVerbose()), 0, daemonParameters.getPeriod());

    }

    // Scheduled read task, tries to read temperature then sends MQTT message
    private static class ScheduledTemperatureReadTask extends TimerTask {
        private NurApi api;
        private MqttMessageSender sender;
        private Timer timer;
        private boolean verbose;
        public ScheduledTemperatureReadTask(NurApi api, MqttMessageSender sender, Timer timer, boolean verbose) {
            this.api = api;
            this.sender = sender;
            this.timer = timer;
            this.verbose = verbose;
        }
        @Override
        public void run() {
            try {
                // Clear inventory
                api.clearIdBuffer(true);

                if (verbose) {
                    System.out.println("rfid.service " + ZonedDateTime.now(ZoneId.of("Europe/Rome")) + " Temperature read");
                }
                // Try reading Magnus S temperature sensors
                boolean notEmpty = false;
                Map<String, Double> temperatureRead = TemperatureSelectRead(api, verbose);
                long time = System.currentTimeMillis();
                JSONArray message = new JSONArray();
                for (Map.Entry<String, Double> e : temperatureRead.entrySet()) {
                    message.put(JSONMessageBuilder.buildMessage(e.getValue(), time, e.getKey()));
                    notEmpty = true;
                }

                // Send MQTT message
                if (notEmpty) {
                    if (verbose) {
                        System.out.println("rfid.service " + ZonedDateTime.now(ZoneId.of("Europe/Rome")) + " Sending message: " + message);
                    }
                    sender.sendMessage("rfidReader", message.toString().getBytes());
                }

            } catch (Exception e) {
                api.dispose();
                sender.terminate();
                timer.cancel();
                timer.purge();
                e.printStackTrace();
                System.exit(1);
            }
        }
    }

    // Try to read temperature from every Magnus-S in range, discard other types of tags
    private static Map<String, Double> TemperatureSelectRead(NurApi api, boolean verbose) throws Exception {

        Map<String, Double> temperatureRead = new TreeMap<>();

        // Filter settings
        NurInventoryExtendedFilter temperatureReadSelect = new NurInventoryExtendedFilter();
        temperatureReadSelect.action = NurApi.FILTER_ACTION_0;
        temperatureReadSelect.address = 0xE0;
        temperatureReadSelect.bank = NurApi.BANK_USER;
        temperatureReadSelect.maskBitLength = 0;
        temperatureReadSelect.targetSession = NurApi.SESSION_SL;
        temperatureReadSelect.truncate = false;

        // Inventory settings
        NurInventoryExtended inventory = new NurInventoryExtended(0, NurApi.SESSION_S1, 0);

        // Set extended carrier, aka leave rfid devices powered up between inventory and read
        api.setExtendedCarrier(true);

        // Execute inventory
        NurRespInventory resp = api.inventoryExtended(inventory, temperatureReadSelect);

        // Wait 3 ms to allow tags to correctly calculate temperature
        Thread.sleep(3);

        // Try to read tags only if there are any
        if (resp.numTagsFound > 0) {
            api.fetchTags();
            NurTagStorage store = api.getStorage();
            // For each tag found
            for (int n=0; n < store.size(); n++) {
                // Read needed data directly from the tag
                try {
                    NurTag tag = store.get(n);
                    byte[] epcb = tag.getEpc(); // Tag's EPC value for operations
                    byte[] epcb4 = new byte[]{epcb[0], epcb[1], epcb[2], epcb[3]};
                    String epc = tag.getEpcString(); // Tag's EPC value for print
                    if (Arrays.equals(epcb4, new byte[]{(byte) 0xE2, (byte) 0x82, (byte) 0x40, (byte) 0x3D})) { // Tag's epc starts with these values = is Magnus S
                        byte[] tempb = tag.readTag(NurApi.BANK_PASSWD, 0xE, 2); // Temperature code
                        byte[] calibrationDataInput = tag.readTag(NurApi.BANK_USER, 0x8, 8); // 64-bit temperature calibration data structure
                        String str = new String(tag.readTag(NurApi.BANK_USER, 0, 8));
                        MagnusS s3tag = new MagnusS(calibrationDataInput);
                        // Do temperature calculation only if calibration data is valid
                        if (s3tag.validate()) {
                            // Print temperature
                            int tempint = new BigInteger(tempb).intValueExact();
                            double temp = s3tag.calculateTemperature(tempint);
                            temperatureRead.put(str, temp);
                        }
                    }
                } catch (Exception e) { // Unfortunately, the NUR API throws Exception for tag reading errors
                    if (verbose) {
                        System.out.println("Error while reading one tag, read continues");
                    }
                }
            }
        }
        api.setExtendedCarrier(false); // Read has concluded, we can turn extended carrier off
        return temperatureRead;
    }

    // Tries to get serial number from /proc/cpuinfo, if no serial found returns null
    private static String getSerial() {
        try {
            File f = new File("/proc/cpuinfo");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            for (String line : br.lines().toList()) {
                if (line.contains("Serial")) {
                    String[] split = line.split(":");
                    return split[split.length - 1].trim();
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File /proc/cpuinfo not found, stopping");
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }
}