package matchwrite;

import com.nordicid.nurapi.*;
import util.APIBuilder;

import java.util.Arrays;

public class MatchWrite {
    public static void main(String[] args) {
        // Parse command line arguments, abort if errors
        MatchWriteParameters matchWriteParameters = new MatchWriteParameters();
        matchWriteParameters.parse(args);
        if (!matchWriteParameters.isCorrect()) {
            System.out.println("Error in parameters, aborting execution");
            return;
        }
        if (matchWriteParameters.getSerialDevice() == null && matchWriteParameters.getSocket() == null) {
            System.out.println("Error: no reader provided, use --socket or --serial");
            return;
        }

        // Connect API
        NurApi api = null;
        try {
            if (matchWriteParameters.getSerialDevice() != null) {
                api = APIBuilder.getAPISerial(matchWriteParameters.getSerialDevice(), matchWriteParameters.isVerbose());
            } else {
                api = APIBuilder.getAPISocket(matchWriteParameters.getSocket(), matchWriteParameters.isVerbose());
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        matchAndWriteTag(api, matchWriteParameters.getMatch(), matchWriteParameters.getWrite());
        api.dispose();
    }

    private static void matchAndWriteTag(NurApi api, String match, String write) {
        try {
            // Clear inventory
            api.clearIdBuffer(true);

            NurRespInventory resp = api.inventory();
            if (resp.numTagsFound > 0) {
                api.fetchTags();
                NurTagStorage store = api.getStorage();
                // For each tag found
                for (int n = 0; n < store.size(); n++) {
                    NurTag t = store.get(n);
                    System.out.println(t.getEpcString());
                    byte[] epcb = t.getEpc(); // Tag's EPC value for operations
                    byte[] epcb4 = new byte[]{epcb[0], epcb[1], epcb[2], epcb[3]};
                    // E282403D at start of epc matches Magnus S tags
                    if (Arrays.equals(epcb4, new byte[]{(byte) 0xE2, (byte) 0x82, (byte) 0x40, (byte) 0x3D})) {
                        if (new String(t.readTag(NurApi.BANK_USER, 0, 6)).equals(match)) {
                            t.writeTag(NurApi.BANK_USER, 0, write.getBytes());
                        }
                        byte[] read = t.readTag(NurApi.BANK_USER, 0, 8);
                        System.out.println(Arrays.toString(read));
                        System.out.println(new String(read));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
