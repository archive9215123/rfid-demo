package matchwrite;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

public class MatchWriteParameters {
    @Option(name="-s", aliases = {"--serial"}, usage = "Define a serial reader to try to use")
    private String serialDevice;

    @Option(name="-sk", aliases = {"--socket"}, usage = "Define an ip:port to connect to as reader")
    private String socket;

    @Option(name = "-v", aliases = {"--verbose"}, usage = "Print detailed log")
    private boolean verbose;

    @Option(name = "-m", aliases = {"--match"}, usage = "String to match in tag", required = true)
    private String match;

    @Option(name = "-w", aliases = {"--write"}, usage = "String to write in tag", required = true)
    private String write;

    private boolean correct = false;

    public void parse(String[] args) {
        CmdLineParser p = new CmdLineParser(this);
        try {
            p.parseArgument(args);
            correct = true;
        } catch (CmdLineException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public String getSerialDevice() {
        return serialDevice;
    }

    public String getSocket() {
        return socket;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public String getMatch() {
        return match;
    }

    public String getWrite() {
        return write;
    }

    public boolean isCorrect() {
        return correct;
    }
}
