package util;

import com.nordicid.nurapi.NurApi;
import com.nordicid.nurapi.NurApiSocketTransport;
import com.nordicid.nurapi.NurApiTransport;
import rfid.java.transports.NurApiSerialTransport;

public class APIBuilder {
    public static NurApi getAPISerial(String serialDevice, boolean verbose) throws Exception {
        NurApiTransport trans = new NurApiSerialTransport(serialDevice, 115200);
        return getAPITransport(trans, verbose);
    }

    public static NurApi getAPISocket(String socket, boolean verbose) throws Exception {
        String[] split = socket.split(":");
        NurApiTransport trans = new NurApiSocketTransport(split[0], Integer.parseInt(split[1]));
        return getAPITransport(trans, verbose);
    }

    private static NurApi getAPITransport(NurApiTransport trans, boolean verbose) throws Exception {
        NurApi api = new NurApi();
        api.setLogToStdout(true);
        if (verbose) {
            api.setLogLevel(NurApi.LOG_ERROR | NurApi.LOG_VERBOSE | NurApi.LOG_USER | NurApi.LOG_DATA);
        }
        api.setTransport(trans);
        api.connect();
        // Set transmission level to max
        api.setSetupTxLevel(1);
        return api;
    }
}
