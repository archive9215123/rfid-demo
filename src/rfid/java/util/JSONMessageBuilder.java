package util;

import org.json.JSONObject;

public class JSONMessageBuilder {
    public static void main(String[] args) {
        buildMessage(10, 10, "a");
    }

    public static JSONObject buildMessage(double temp, long timestamp, String tag) {
        JSONObject obj = new JSONObject();
        obj.put("timestamp", timestamp);
        obj.put("value", String.valueOf(temp));
        obj.put("tag", tag);
        obj.put("type", "Float32");
        return obj;
    }
}
