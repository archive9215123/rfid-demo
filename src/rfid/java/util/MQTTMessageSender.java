package util;

import org.eclipse.paho.mqttv5.client.IMqttToken;
import org.eclipse.paho.mqttv5.client.MqttAsyncClient;
import org.eclipse.paho.mqttv5.client.MqttConnectionOptions;
import org.eclipse.paho.mqttv5.client.persist.MemoryPersistence;
import org.eclipse.paho.mqttv5.common.MqttException;
import org.eclipse.paho.mqttv5.common.MqttMessage;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/*
* Provides a service that uses an independent thread to send mqtt
* messages to a broker of choice
* */

public class MqttMessageSender {
    private String baseTopic;
    private int qos;
    private String broker;
    String clientID;
    MemoryPersistence persistence;
    MqttConnectionOptions options;
    MqttAsyncClient client;
    ExecutorService executor;

    public static MqttMessageSender defaultSender() throws MqttException {
        MqttMessageSender sender = new MqttMessageSender("rpi/temperature/", 2, "tcp://localhost:1883", "raspberry", false);
        return sender;
    }

    public MqttMessageSender(String baseTopic, int qos, String broker, String clientID, boolean cleanStart) throws MqttException {
        this.baseTopic = baseTopic;
        this.qos = qos;
        this.broker = broker;
        this.clientID = clientID;
        persistence = new MemoryPersistence();
        options = new MqttConnectionOptions();
        options.setCleanStart(cleanStart);
        client = new MqttAsyncClient(this.broker, clientID, persistence);
        executor = Executors.newSingleThreadExecutor();
    }

    public void sendMessage(String topic, byte[] content) {
        MessageTask task = new MessageTask(topic, content);
        executor.execute(task);
    }

    public void terminate() {
        executor.shutdown();
        while (!executor.isTerminated()) {
            try {
                executor.awaitTermination(1000, TimeUnit.HOURS);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            }
        }
        try {
            client.close();
        } catch (MqttException me) {
            me.printStackTrace();
        }

    }

    class MessageTask implements Runnable {
        byte[] content;
        String topic;
        public MessageTask(String topic, byte[] content) {
            this.content = content;
            this.topic = baseTopic + topic;
        }
        @Override
        public void run() {
            try {
                IMqttToken token = client.connect();
                token.waitForCompletion();
                MqttMessage message = new MqttMessage(content);
                message.setQos(qos);
                token = client.publish(topic, message);
                token.waitForCompletion();
                client.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }
}
