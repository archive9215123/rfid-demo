package util;

import java.math.BigInteger;

// Magnus-S tag data manipulation
public class MagnusS {

    // Fields
    private int crc;
    private byte[] raw;
    private int code1;
    private int temp1;
    private int code2;
    private int temp2;
    private int version;

    // constructor from byte array
    public MagnusS(byte[] calibrationData) {
        // Extract calibration data from the structure
        // First 16 bits are CRC-16 of the following 48
        byte[] crcb = new byte[] {calibrationData[0], calibrationData[1]};
        crc = new BigInteger(crcb).intValueExact();
        // Saving raw data to validate with CRC-16
        raw = new byte[] {calibrationData[2], calibrationData[3], calibrationData[4], calibrationData[5], calibrationData[6], calibrationData[7]};
        // CODE1, 12 bits
        byte[] code1b = {calibrationData[2], calibrationData[3]};
        code1 = new BigInteger(code1b).intValueExact();
        code1 = (code1 & 0xFFF0) >> 4;
        // TEMP1, 11 bits
        byte[] temp1b = {calibrationData[3], calibrationData[4]};
        temp1 = new BigInteger(temp1b).intValueExact();
        temp1 = (temp1 & 0x0FFE) >> 1;
        // CODE2, 12 bits
        byte[] code2b = {calibrationData[4], calibrationData[5], calibrationData[6]};
        code2 = new BigInteger(code2b).intValueExact();
        code2 = (code2 & 0x1FFE0) >> 5;
        // TEMP2, 11 bits
        byte[] temp2b = {calibrationData[6], calibrationData[7]};
        temp2 = new BigInteger(temp2b).intValueExact();
        temp2 = (temp2 & 0x1FFC) >> 2;
        // Version number, 2 bits, always 0
        byte[] versionb = {calibrationData[7]};
        version = new BigInteger(versionb).intValueExact();
        version = version & 0x3;
    }

    // Validate data with CRC-16
    public boolean validate() {
        int crc16 = CRC16.crc16(raw);
        return crc16 == crc;
    }

    // Calculate calibrated temperature from temperature code
    public double calculateTemperature(int temperatureCode) {
        return (((temp2 - temp1 + 0.0) / (code2 - code1)) * (temperatureCode - code1) + temp1 - 800)  / 10;
    }

    @Override
    public String toString() {
        String r = "";
        r = r + "Calibration data:\n";
        r = r + "1:" + temp1 + " " + code1 + " " + ((temp1 - 800) / 10.0) + "\n";
        r = r + "2:" + temp2 + " " + code2 + " " + ((temp2 - 800) / 10.0) + "\n";
        return r;
    }

    // GETTERS

    public int getCrc() {
        return crc;
    }

    public int getCode1() {
        return code1;
    }

    public int getTemp1() {
        return temp1;
    }

    public int getCode2() {
        return code2;
    }

    public int getTemp2() {
        return temp2;
    }

    public int getVersion() {
        return version;
    }
}
